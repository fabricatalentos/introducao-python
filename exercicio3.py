print()
print('Bem vindo a Loja Vinicera Automoveis!')
print()


veiculos = [
     {'marca': 'Volkswagen', 'modelo':'Gol', 'preco': 60000},
     {'marca': 'Hyundai', 'modelo':'HB20', 'preco': 60000},
     {'marca': 'Chevrolet ', 'modelo':'Onix', 'preco': 60000},
     {'marca': 'Fiat ', 'modelo':'Mobi', 'preco': 60000},
     {'marca': 'Fiat ', 'modelo':'Argo', 'preco': 60000}
]

while True:
    print ('1. Caso queira compar um carro')
    print ('2. Caso queira cadastrar um carro')
    print ('3. Caso queira listar os carros disponiveis')
    print ('4. Caso queira sair')

    print()
    choice = int(input("-> Digite a opcao: "))
    print()



    if choice == 1:
        print("Carros disponíveis para compra:")
        for cont, elemento in enumerate(veiculos):
            print(f'{cont + 1} - {elemento['marca']} {elemento['modelo']} {elemento['preco']}')
        print()

        carroComprado = int(input('Informe o indice do carro que queira comprar:'))
        if 0 < carroComprado <= len(veiculos):
            del veiculos[carroComprado - 1]
            print('Carro Comprado! Parabens!')
        else:
            print("Opção inválida! Por favor, digite um número de carro válido.")

    elif choice == 2:
        aux = int(input('Digite quantos carros deseja cadastrar: '))

        for count in range(1,aux+1):
            marca = input('Digite a marca do carro:')
            modelo = input('Digite a modelo do carro:')
            preco = float(input('Digite o preco do carro:'))

            veiculos.append({'marca': marca, 'modelo': modelo, 'preco': preco})

            print("Carro adicionado!")
            print()

    elif choice == 3:
        print("Carros disponíveis para compra:")
        for cont, elemento in enumerate (veiculos):
            print(f'{cont+1} - {elemento['marca']} {elemento['modelo']} {elemento['preco']}')
        print()

    else:
        print('Até a próxima!')
        break