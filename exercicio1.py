
#--------------------------Exercicio 1----------------------------

numero1 = float(input('Entre com o primeiro numero: '))
numero2 = float(input('Entre com o segundo numero: '))

if numero1 > numero2:
    print('O maior número é o primeiro: ',numero1)

elif numero1 < numero2:
    print('O maior número é o segundo: ',numero2)

else:
    print('Os números são iguais')

#-----------------------------------------------------------------

#--------------------------Exercicio 2----------------------------
desempenhoAtual = float(input('Entre com a receita do mês atual: '))
desempenhoPassado = float(input('Entre com a receita mês anterior: '))
resultado = (desempenhoAtual-desempenhoPassado)/desempenhoPassado

if desempenhoAtual > desempenhoPassado:
    print('Desempenho foi positivo em: ',resultado*100, '%')

elif desempenhoAtual < desempenhoPassado:
    print('Desempenho foi negativo em: ',resultado*100, '%')

else:
    print('O desempenho se manteve')


