
#--------------------------Exercicio 1----------------------------
numero1 = int(input('Entre com o primeiro numero inteiro:'))
numero2 = int(input('Entre com o segundo numero inteiro:'))

if numero1 > numero2:
    for cont in range(numero2, numero1+1):
        print(cont)

if numero1 < numero2:
    for cont in range(numero1, numero2+1):
        print(cont)

#-----------------------------------------------------------------

#--------------------------Exercicio 2----------------------------
numero1 = int(input('Entre com o primeiro numero inteiro:'))
numero2 = int(input('Entre com o segundo numero inteiro:'))

if (numero1 and numero2) > 0 and (numero1 and numero2) < 11:
    resultado = numero1*numero2;
    print("Resultado Taboada: ", resultado)
